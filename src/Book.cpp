/**
 * Her bir kitabın ana hafızada tutulacağı 
 * yapı.
 * 
 * Detaylı bilgi Book.h dosyasındadır.
 *
 **/

#include "../include/DATARULES.h"
#include "../include/Book.h"
#include <iostream>

std::string Book::fill_string(std::string str, const int size){
  
  std::string copy_str = str;
  int len = str.size();
  while (len < size){
    copy_str += BLANK_CHAR;
    len++;
  }
  return copy_str;
  
}

std::string Book::strip_str(std::string str){

    unsigned int len = str.length();
    register int i = 0;
    while (str[i] != BLANK_CHAR){ // FIXME: verinin tam doldurmadığından emin olunmalı !!
	i++;
    }

    return str.substr(0, i);
}


Book::Book(){
  this->state="n";
}

Book::Book(std::string isbn, std::string name, std::string author, std::string cost){
  this -> state = "n"; // 'normal' anlamında (varsayılan)
  this -> isbn = isbn;
  this -> name = name;
  this -> author = author;
  
  if (cost[0] == '$')
      cost = cost.substr(1, cost.length());

  this -> cost = cost;
}

std::string Book::serialize(){
  std::string serial;

  serial += fill_string(this->state, STATE_SIZE);
  serial += fill_string(this->isbn, ISBN_SIZE);
  serial += fill_string(this->name, NAME_SIZE);
  serial += fill_string(this->author, AUTHOR_SIZE);
  serial += fill_string(this->cost, COST_SIZE);
    
    
  return serial;
}

void Book::deserialize(std::string serialized){
  
    this->state  = strip_str(serialized.substr(0,1));
    this->isbn   = strip_str(serialized.substr(1, 20));
    this->name   = strip_str(serialized.substr(21, 200));
    this->author = strip_str(serialized.substr(221, 100));
    this->cost   = strip_str(serialized.substr(321, 10));
}

std::string Book::get_state(){return this->state;};
std::string Book::get_isbn(){return this->isbn;};
std::string Book::get_name(){return this->name;};
std::string Book::get_author(){return this->author;};
std::string Book::get_cost(){return this->cost;};

void Book::set_isbn(std::string val){this->isbn = val;}
void Book::set_name(std::string val){this->name = val;}
void Book::set_author(std::string val){this->author = val;}
void Book::set_cost(std::string val){this->cost = val;}
