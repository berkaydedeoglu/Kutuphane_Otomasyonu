/**
 * İkili dosyaya erişimi diğer sınıflardan soyutlayan sınıf.
 * 
 * Detaylı bilgi BooksIOHelper.h dosyasındadır.
 *
**/

#include "../include/BooksIOHelper.h"
#include "../include/DATARULES.h"
#include <string>
#include <iostream>
#include <cstdio>

BooksIOHelper::BooksIOHelper(){
    this->open_file();
    this->BUFFER_SIZE = buffer_size();
    this->total_book_count = get_count();
}

int BooksIOHelper::buffer_size(){
    int size = STATE_SIZE +
	       ISBN_SIZE +
	       NAME_SIZE +
	       AUTHOR_SIZE +
	       COST_SIZE;

    return size;
}

bool BooksIOHelper::open_file(){

    this->book_file.open(DATAFILENAME,
			 std::ios::binary | std::ios::out |
			 std::ios::in | std::ios::ate);

   
    if (book_file.is_open()){ 
        return true;
    }
    else return false;
}

void BooksIOHelper::go_record(unsigned int record_index){
    int record_byte;
    record_byte = record_index * this->BUFFER_SIZE;

    if (record_index < total_book_count)
	book_file.seekg(record_byte);
}

unsigned int BooksIOHelper::compress_file(){
    std::ofstream temp_file("../data/t_Books.bin", std::ios::binary);
    register char* record = new char[BUFFER_SIZE];

    register int i = 0;
    while (i < total_book_count){
	this->go_record(i);
	book_file.get(record[0]);
	if(record[0] == 'n'){
	    book_file.read(record+1, BUFFER_SIZE-1);
	    temp_file.write(record, BUFFER_SIZE);
	}
	i++;
    }
    delete[] record;


    temp_file.flush();

    book_file.close();
    temp_file.close();

    std::remove(DATAFILENAME.c_str());
    std::rename("../data/t_Books.bin", DATAFILENAME.c_str());

    // Constructor yeniden yazılır.
    this->open_file();
    this->total_book_count = get_count();
    return 1;
}

Book BooksIOHelper::get_record(unsigned int index){

  char* data_str = new char[BUFFER_SIZE];
  Book book;
  
  this->go_record(index);

  book_file.read(data_str, BUFFER_SIZE);
  book.deserialize(data_str);

  delete[] data_str;
  return book;
}

std::string BooksIOHelper::get_just_name(unsigned int index){
    std::string name;
    char * temp_data = new char[NAME_SIZE];
    
    this->go_record(index); // Kaydın başı

    int data_byte = (int)book_file.tellg()+STATE_SIZE+ISBN_SIZE;
    book_file.seekg(data_byte); //Kaydın ortası

    book_file.read(temp_data, NAME_SIZE);

    name = Book::strip_str(temp_data);

    delete[] temp_data;

    return name;
}

std::string BooksIOHelper::get_just_isbn(unsigned int index){
    std::string name;
    char * temp_data = new char[ISBN_SIZE];
    
    this->go_record(index); // Kaydın başı
    int data_byte = (int)book_file.tellg()+STATE_SIZE;
    book_file.seekg(data_byte); //Kaydın ortası

    book_file.read(temp_data, ISBN_SIZE);

    name = Book::strip_str(temp_data);

    delete[] temp_data;

    return name;
}

unsigned int BooksIOHelper::get_count(){
  int count;
  int current_position;

  current_position = book_file.tellg();
  
  book_file.seekg(0, std::ios::end);
  count = book_file.tellg();
  book_file.seekg(current_position);

  return count/BUFFER_SIZE;
}

unsigned int BooksIOHelper::get_deleted_count(){
    char state;
    int total = this -> get_count();

    register int i = 0;
    unsigned int count = 0;
    while (i<total){
	this -> go_record(i);
	book_file.get(state);
	if (state == 'd')
	    count++;
	i++;
    }

    return count;
}

void BooksIOHelper::add_book(Book book){
  book_file.seekg(total_book_count*BUFFER_SIZE);
  book_file << book.serialize();
  this->total_book_count++;
  book_file.flush();
}

void BooksIOHelper::write_book(Book book, unsigned int index){
  go_record(index);
  book_file << book.serialize();
  book_file.flush();
}

void BooksIOHelper::delete_book(unsigned int index){
  go_record(index);
  book_file << 'd';
  book_file.flush();
}

void BooksIOHelper::swap_records(unsigned int index_1, unsigned int index_2){
    Book book_1;
    Book book_2;
    book_1 = this->get_record(index_1);
    book_2 = this->get_record(index_2);

    this->write_book(book_1, index_2);
    this->write_book(book_2, index_1);
    book_file.flush();
}

void BooksIOHelper::save_file(){
    book_file.flush();
}

void BooksIOHelper::close_file(){
  //this->compress_file();
  book_file.close();
}

