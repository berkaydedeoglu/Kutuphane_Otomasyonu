/**
 * Tüm sistemin ayağa kaldırıldığı ve kullanıcı etkileşimini sağlayan fonksiyonların
 * bulunduğu dosya.
 *
 * Dosyadaki tüm fonksiyonlar aptaldır, ayrıca açıklanmamıştır. Yalnızca veri alır
 * ve commands nesnesinin fonksiyonlarını kullanılır.  
 *
 *
 * Yazar: Berkay Dedeoğlu
 * Tarih: 12.03.2018
 **/


#include <iostream>
#include <string>
#include "../include/Book.h"
#include "../include/Commands.h"
#include "../include/PlaintoBinary.h"



void print_menu(int);   // Seçme menüsünü yazdırmak.
int user_type();        // Yönetici ya da normal kullanıcı seçimi 
void create_book();     // Yeni kitap oluşturmak ve onu dosyaya eklemk
void find_by_isbn();    // ISBN'e göre ikili dosyada veri arama
void find_by_name();    // İsme göre ikili dosyada veri arama
void delete_record();   // İkili dosyada istenen kaydın sanal olarak silinmesi
void sort_records();    // İstenilen tipe göre ikili dosyanın sıralanması
void bad_choice();      // Yanlış ya da eksik bir seçim yapılması durumunda bunun kullanıcıya gösterilmesi
void update_record();   // İkili dosyadaki verinin bir özelliğini değiştirme.



/**
 * Yukarıdaki tüm fonksiyonların kullandığı temel sınıf. 
 * Bu dosyadaki hiçbir metod dosyaya doğrudan erişim sağlamaz,
 * Commands sınıfını kullanır.
 **/
Commands commands;




int main(){

  /*
    
  // Text dosyasını binary dosyasına yazmak için yorumu kaldırın.
  
  PlaintoBinary ptb;
  ptb.write_to_data_file();
  return 1;
  
  */
  
  char type = user_type();
  

  while (true){
    print_menu(type);
    char val;
    std::cin >> val;

    
    switch (val){
      case '1': 
        commands.print_books();
        break;
      case '2':
        find_by_isbn();
        break;
      case '3':
        find_by_name();
        break;
      case '4':
        create_book();
        break;
      case '5':
        delete_record();
        break;
      case '6':
        update_record();
        break;
      case '7':
        sort_records();
        break;
      case 'q':
        commands.prepare_quit();
        return 1;
      default:
	  bad_choice();  
    }

    
    // Devam etmek için bir tuşa basın komutları
    char temp;
    std::cin.ignore();
    std::cout << std::endl << std::endl << std::endl << std::endl;
    std::cout << "Devam etmek için bir <Enter> basın :)"; 
    std::cin.getline(&temp, 1);
    std::cin.clear();
    // Devam etmek için bir tuşa basın komutları
    
  }

  return 0;
}

int user_type(){
  char type;

  std::cout << "1. Normal" << std::endl;
  std::cout << "2. Yonetici" << std::endl; 

  std::cin >> type;
  return type;
}

void print_menu(int type){
  std::cout << "====================================" << std::endl;

  std::cout << "1. Kayitlari goruntule" << std::endl;
  std::cout << "2. ISBN'e gore arama" << std::endl;
  std::cout << "3. Kitap adina gore arama" << std::endl;

  if (type == '2'){
    std::cout << "4. Kayit ekle" << std::endl;
    std::cout << "5. Kayit sil" << std::endl;
    std::cout << "6. Kaydi Guncelle" << std::endl;
    std::cout << "7. Kayitlari sirala" << std::endl;
  }

  std::cout << "Cikmak icin 'q' basin ..." << std::endl;

  std::cout << "====================================" << std::endl;
}

void create_book(){
  std::cout << "ISBN no : ";
  std::string isbn;
  std::cin >> isbn;

  std::cout << "Isim : ";
  std::string name;
  std::cin >> name;

  std::cout << "Yazar : ";
  std::string author;
  std::cin >> author;

  std::cout << "Fiyat : ";
  std::string cost;
  std::cin >> cost;

  Book book(isbn, name, author, cost);

  commands.add_book(book);
}

void find_by_isbn(){
  std::string isbn;
  std::cout << "ISBN numarasi girin: ";
  std::cin >> isbn;
  int id = commands.find_book(Command_Type::by_isbn, isbn);
  if (commands.get_book(id).get_state() == "d"){
    std::cout << "Bu Kitap silinmistir!!!"<<std::endl;
  }else{
    std::cout << "Aradiginiz kaydin sıra nosu: " << id << std::endl;
  }
  
}

void find_by_name(){
    std::string name;
    
    std::cout << "Kitap ismini girin: ";
    std::cin.ignore();
    std::getline(std::cin, name);
    int id = commands.find_book(Command_Type::by_name, name);
    std::cout << "Aradiginiz kaydin sıra nosu: " << id << std::endl;
}

void delete_record(){
  std::string val;
  std::cout << "Silinecek kitap sira numarasi: ";
  std::cin >> val;
  int id  = std::stoi(val);
  commands.delete_book(id);
}

void sort_records(){
  if (commands.any_deleted_books()){
    std::cout << "Silinmis kayitlar var ve sizin bu islemi yapmaniz";
    std::cout << "kayit dosyasina zarar verebilir."<<std::endl;
    std::cout << "Veriler yeniden siralansin mi?(e,h)";

    char validate;
    std::cin >> validate;
    
    if (validate == 'e'){
      commands.compress_books();
      std::cout << "Islem tamamlandi. siralari kontrol edin"<< std::endl;
    }else if (validate == 'h'){
      std::cout << "Siralama islemi yapilmayacak" << std::endl;
      return;
    }else{
      bad_choice();
      return;
    }

  }
  std::cout << "1. ISBN'e gore sirala"<< std::endl;
  std::cout << "2. Isme gore sirala"<< std::endl;
  char choice;
  std::cin >> choice;

  if (choice == '1'){
    commands.sort_books(Command_Type::by_isbn);
  }else if (choice == '2'){
    commands.sort_books(Command_Type::by_name);
  }else{
    bad_choice();
  }
}

void bad_choice(){
  std::cout << "Eksik ya da hatali bir secim yaptiniz!!!"<<std::endl;
}

void update_record(){
  std::cout << "Guncellenmesini istediginiz kaydin sıra numarası: ";
  std::string id;
  std::cin >> id;
  Book book = commands.get_book(std::stoi(id));
  
  if (book.get_state() == "d"){
    std::cout << "Bu kayit silinimistir !!!"<< std::endl;
    return;
  }
  
  std::cout << "1. ISBN: " << book.get_isbn() << std::endl;
  std::cout << "2. ISIM: " << book.get_name() << std::endl;
  std::cout << "3. YAZAR: " << book.get_author() << std::endl;
  std::cout << "4. FIYAT: $" << book.get_cost() << std::endl;
  
  std::string choice;
  std::cout << "Seciminiz: ";
  std::cin >> choice;

  std::string value;
  std::cout << "Yeni hali: ";
  std::cin.ignore();
  std::getline(std::cin, value);

  switch (std::stoi(choice)){
    case 1: book.set_isbn(value);
      break;
    case 2: book.set_name(value);
      break;
    case 3: book.set_author(value);
      break;
    case 4: book.set_cost(value);
      break;

    default: bad_choice();
      break;
  }

  commands.update_book(std::stoi(id), book);
  std::cout << "Kayit basariyla guncellenmistir!!" << std::endl;
}
