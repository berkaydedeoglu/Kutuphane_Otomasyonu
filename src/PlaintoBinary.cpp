/**
 * Plain text formatındaki hazır kitap kayıtlarını
 * binary dosyaya yazan sınıf.
 *
 * Detaylı bilgi için PlainToBinary.h dosyasına bakın.
 **/

#include "../include/PlaintoBinary.h"
#include "../include/DATARULES.h"



PlaintoBinary::PlaintoBinary() {
  textFile.open(TEXTFILENAME);
  dataFile.open(DATAFILENAME,  std::ios::out | std::ios::binary);
}

int PlaintoBinary::book_count() {
  // Toplam \n değeri 5'e de bölünebilir.
  
  register char c;
  register char oldc;
  register int count;

  while (textFile.get(c)){
    if (c == '\n' && oldc == '\n')  // Ardarda 2 \n varsa yeni kitap 
      count++;
    oldc = c;
  }

  // Son kitabın sonunda dosya bittiğinden '\n' karakteri yok.
  // Bu yüzden sayılan sayıya 1 eklenir.
  count++;  
  
  return count; 
}

void PlaintoBinary::text_file_begin(){
  textFile.clear();
  textFile.seekg(0, std::ios::beg);
}

std::string * PlaintoBinary::parse_a_book(){
  std::string * bookArr = new std::string[4];

  std::getline(textFile, bookArr[0]);  // ISBN numarası
  std::getline(textFile, bookArr[1]);  // Kitap İsmi
  std::getline(textFile, bookArr[2]);  // Yazar Adı
  std::getline(textFile, bookArr[3]);  // Fiyat

  std::string temp;
  std::getline(textFile, temp); // Her kitabın altındaki boş satır.

  return bookArr;
}

Book PlaintoBinary::create_a_book(std::string strBook[4]){
  Book book(strBook[0], strBook[1],
	    strBook[2], strBook[3]);

  delete[] strBook;  // Hafıza serbest bırakılmalı.

  return book;
}

void PlaintoBinary::write_a_book(Book book){
  std::string str = book.serialize();  // 331 karakterli (byte) kitap stringi. Bkz. DATARULES.h
  dataFile << str;
}

void PlaintoBinary::close_files(){
  textFile.close();
  dataFile.close();
}

void PlaintoBinary::write_to_data_file(){
  int count = book_count();
  text_file_begin(); // Sayımdan sonra dosya göstericisi en başa alınır.
  Book tempBook;
  
  int i = 0;
  while (i < count){
    std::string * arr = parse_a_book();
    tempBook = create_a_book(arr);	
    write_a_book(tempBook);
    i++;
  }

  close_files();
}





