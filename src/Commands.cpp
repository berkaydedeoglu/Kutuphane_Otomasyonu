/**
 * BooksIOHelper sınıfının atomik işlemlerinden 
 * daha kompleks işlemler yapılmasını sağlayan 
 * sınıf
 *
 * Detaylı bilgi için Commands.h dosyasına bakın.
 **/

#include "../include/Commands.h"
#include <iostream>


Commands::Commands(){
    //default const
}

unsigned int Commands::smallest(Command_Type type,
                                unsigned int from,
                                unsigned int to){

    std::string smallest = "";
    std::string temp;
    unsigned int smallest_index;
    int i = from;
    
    while (i <= to) {
        if (type == Command_Type::by_isbn){
            temp = books.get_just_isbn(i);
        }else{
            temp = books.get_just_name(i);
        }

        if (temp < smallest || smallest == ""){
            smallest = temp;
            smallest_index = i;
        }
        
        i++;
    }

    return smallest_index;
}

int Commands::linear_search_by_isbn(std::string isbn){
    int count = books.get_count();
    int i = 0;

    while (i < count){
	if (books.get_just_isbn(i) == isbn){
	    if (books.get_record(i).get_state() == "n")
		return i;
	}
	i++;
    }

    return -1;
}

bool Commands::are_books_sorted(Command_Type type){
    int limit = books.get_count();
    int i = 0;

    if (type == Command_Type::by_isbn){
        
        while (i < limit-1){
            if (books.get_just_isbn(i) > books.get_just_isbn(i+1)){
                return false;
            }
            i++;

        }
    }else{
        while (i < limit-1){
            if (books.get_just_name(i) > books.get_just_name(i+1)){
                return false;
            }
            i++;

        }
    }
    return true;
}

void Commands::sort_books(Command_Type type){
    
    if(this->are_books_sorted(type)){
        return;
    }

    std::cout << "Sıralanıyor..." << std::endl;

    int i = 0;
    int length = books.get_count();
    int temp_smallest;
    while (i < length){
        temp_smallest = smallest(type, i, length-1);
        books.swap_records(i, temp_smallest);
        i++;
    }
    books.save_file();
    
    std::cout << "Sıralandı" << std::endl;
       
}

unsigned int Commands::find_book(Command_Type type, std::string value){
    if (are_books_sorted(type)){
        unsigned int left_index = 0;
        unsigned int right_index = books.get_count()-1;
        unsigned int middle_index;
        unsigned int i = 0;

        while (left_index <= right_index){
            middle_index = (unsigned int) ((left_index+right_index)/2);
            std::string mid_value;
            
            if (type == Command_Type::by_isbn){
                mid_value = books.get_just_isbn(middle_index);
            }else{
                mid_value = books.get_just_name(middle_index);
            }

            if (mid_value == value){
                return middle_index;
            }else if(value > mid_value){
                left_index = middle_index+1;
            }else if(value < mid_value){
                right_index = middle_index-1;
            }
        }
    }else{
        std::cout << "Veriler sirali degil!"<<std::endl; 
        this->sort_books(type);
        return find_book(type, value);
    }
    return -1;
}

void Commands::add_book(Book book){
    int already_exist = linear_search_by_isbn(book.get_isbn());
    if (already_exist >= 0){
	std::cout << "Bu kayıt zaten mevcut. Sıra no: " << already_exist;
	std::cout << std::endl;
	return;
    }

    
    if (deleted_books.empty()){
        books.add_book(book);
    }else{
        books.write_book(book, deleted_books.top());
        deleted_books.pop();
    }
}

Book Commands::get_book(unsigned int book_id){
    return books.get_record(book_id);
}

void Commands::update_book(unsigned int book_index, Book new_book){
    books.write_book(new_book, book_index);
}

bool Commands::any_deleted_books(){
    if (deleted_books.empty()){
        return false;
    }else {
        return true;
    }
}

void Commands::delete_book(unsigned int index){
    books.delete_book(index);
    deleted_books.push(index);
}

void Commands::print_books(){
    unsigned int limit = books.get_count();
    Book temp_book;
    int i = 0;
    while (i < limit){
        temp_book = books.get_record(i);

        if (temp_book.get_state() == "d"){
            i++;
            continue;
        }

        std::cout << "\n============Sira:" << i << "================" << std::endl;
        std::cout << "Isbn : "  <<temp_book.get_isbn()   << std::endl;
        std::cout << "Isim : "  <<temp_book.get_name()   << std::endl;
        std::cout << "Yazar : " <<temp_book.get_author() << std::endl;
        std::cout << "Fiyat : $" <<temp_book.get_cost()  << std::endl;

        i++;
    }
    
}

unsigned int Commands::book_count(){
    return books.get_count() - books.get_deleted_count();
}

void Commands::compress_books(){
    books.compress_file();
    while (!deleted_books.empty()){
        deleted_books.pop();
    }
}

void Commands::prepare_quit(){
    books.compress_file();
    books.close_file();
}
