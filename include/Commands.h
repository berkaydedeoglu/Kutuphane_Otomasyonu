/**
 * Yazar: Berkay Dedeoğlu
 * Tarih: 05.03.2018
 **/

#ifndef COMMANDS_H
#define COMMANDS_H

#include "BooksIOHelper.h"
#include "Book.h"
#include <string>
#include <stack>


/**
 * Sıralama ve arama işlemleri isme göre ya da isbn numarasına göre 
 * yapılır. 
 *
 * Bu işleri yapan metodlara parametre olarak gönderilecek enum. 
 **/
enum Command_Type {by_name, by_isbn};




/**
 * Verilerin tutulduğu dosyaya doğrudan erişmeden veriler üzerinde 
 * karmaşık işlemleri yapan sınıftır.
 *
 * Kullanıcı arayüzünden alınan bilgiler direkt bu sınıfın metodlarına aktarılır.
 * 
 * Dosyaya yalnızca BooksIOHelper sınıfı üzerinden erişilir. O sınıfın atomik 
 * erişimleriyle daha karmaşık işlemler gerçekleştirilir.
 *
 *--NOT--
 *   Bu sınıfın yalnız çalışması singleton patterniyle sağlanabilirdi ancak 
 *   ödev kapsamında karışıklık sağlayacağından gerek duyulmadı.
 **/
class Commands{

 
 private:
   BooksIOHelper books;
   std::stack <unsigned int>  deleted_books; // Silinenler listesi

  /**
   * Kitapların dosyada sıralı olup olmadığını denetleyen metod.
   * arama işlemleri binary search ile yapıldığından bu metod 
   * gereklidir.
   *
   * --Input--
   *    Command_Type type: Verilerin hangi açıdan sıralı olduğunun
   *                       belirtildiği parametre.
   *
   * --Return--
   *    bool: Verilerin sıralı olduğu ya da olmadığı durum. (True/False)
   **/
   bool are_books_sorted(Command_Type type);

  /**
   * Verilen aralıkta ve verilen tipte en küçük verinin sıra numarasını
   * döndüren metod. 
   * Sıralama işleminde selection sort kullanıldığı için bu metod gereklidir.
   *
   * --Inputs--
   *    Command_Type type:  Hangi veri tipine göre verinin bulunacağını belirten parametre.
   *    unsigned int from:  Aramanın hangi sıradan başlayacağını belirten parametre.
   *    unsigned int to  :  Aramanın hangi sırada biteceğini belirten parametre.
   *
   * --Return--
   *    unsigned int : İstenilen şartlardaki en küçük kaydın sıra numarası (index).
   **/
   unsigned int smallest(Command_Type type, 
                        unsigned int from,
                        unsigned int to);

  /**
   * Yalnızca isbn numarasına göre lineer arama yapan metod.
   * Bu metodun yerine binary search yapan 'find_book' metodu
   * kullanılmadı çünkü yeni eklenen bir kayıt için sıralama
   * yapmak mecrburi tutulmadı.
   *
   * Yeni Book nesnesi eklendiğinde aynı isbn numarasına sahip
   * başka bir kaydın varlığını denetlemek için oluşturuldu.
   *
   * --Input--
   *    std::string isbn : Aranacak isbn numarası
   *
   * --Return--
   *    int : Bulunursa bulunan kaydın sıra numarası
   *          Bulunmazsa -1
   **/
   int linear_search_by_isbn(std::string isbn);
 
 public:
  /**
   * Özelliklerin ilklendirilmesi
   **/
   Commands();


  /**
   * Kayıtlarda bir kitabı bulmayı sağlayan metod. Binary search yöntemini kullanır.
   * Metodun doğru çalışması için kayıtların sıralı olması gerekir.
   *
   * Eğer sıralı değilse konsola uyarı mesajı yollar.
   *
   * --Inputs--
   *    Command_Type type: isbn ya da isme göre seçeneklerinden biri.
   *    std::string value: Aranması istenen veri.
   *
   * --Return--
   *    unsigned int: Bulunan verinin sıra numarası. Eğer veri bulunmamıssa
   *                  -1 döner.
   **/
   unsigned int find_book(Command_Type type, std::string value);

  
  /**
   * Selection sort yöntemiyle kayıtların istenilen tipte (isbn'e göre/isime göre)
   * sıralayan metod.
   *
   * --Inputs--
   *    Command_Type type: Verilerin hangi tipe göre sıralanacağını belirten parametre.
   *
   **/
   void sort_books(Command_Type type);


  /**
   * Kayıt dosyasına yeni kitap eklemeyi sağlayan metod.
   * Eğer mevcut oturumda silinen dosya varsa yeni veri silinenlerin 
   * yerie eklenir.
   *
   * --Inputs--
   *    Book book: Eklenmesi istenen kitap nesnesi.
   **/
   void add_book(Book book);


  /**
   * Kayıt dosyasından kitap nesnesi alan metod.
   *
   * --Input--
   *    unsigned int book_id: Alınacak kitabın sıra numarası.
   *
   * --Return--
   *    Book: Alınan verinin Book nesnesi. 
   **/
   Book get_book(unsigned int book_id);


  /**
   * Kayıt dosyasındaki bir verinin güncellenmesini sağlayan
   * metod.
   *
   * --Inputs--
   *   unsigned int book_index: Değiştirilmesi istenen kitabın sıra 
   *                  numarası.
   *   Book new_book          : Değişecek kitabın yeni hali.
   **/
   void update_book(unsigned int book_index, Book new_book);

  /**
   * Kayıt dosyasındaki istenilen verinin silinmiş olarak 
   * işaretlenmesini sağlayan metod. Bu metod veriyi fiziksel
   * olarak silmez. Sadece durumunu silinmiş olarak değiştirir.
   *
   * --Input--
   *    unsigned int index: Silinecek verinin sıra numarası.
   **/
   void delete_book(unsigned int index);


  /**
   * Silinenler listesinde eleman bulunup bulunmadığını
   * kontrol eden metod.
   *
   * --Return--
   *    bool: Eğer veri varsa true, yoksa false.
   **/
   bool any_deleted_books();


  /**
   * Fiziksel diskteki verileri standart çıktı 
   * birimine düzenli biçimde aktaran metod.
   **/
   void print_books();


  /**
   * Fiziksel diskte silinmemiş kaç veri olduğunu 
   * hesaplayan metod.
   *
   * --Return--
   *    unsigned int: Silinmemiş kitap sayısı.
   **/
   unsigned int book_count();


  /**
   * Silinen verilerin fiziksel bellekten de silinmesini sağlayıp
   * dosyanın sıkıştırılmasını sağlayan metod.
   **/
   void compress_books();

  /**
   * Nesnenin sonlanmaya hazırlığını sağlayan metod. Dosyayı sıkıştırır
   * ve dosyaları kapatır.
   **/
   void prepare_quit(); 
  
};

#endif
