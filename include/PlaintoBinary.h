#ifndef PLAINTOBINARY_H
#define PLAINTOBINARY_H

#include <string>
#include <fstream>
#include "Book.h"



/**
 * İlk çalışmada verilmiş olan '*.txt' dosyasını, 
 * veritabanı olarak kullanılacak binary dosyaya 
 * aktaracak iş sınıfı.
 *
 * Genellikle bir sistemde ilk çalışmada ya da hazır
 * sistemde binary dosyanın bozulması durumunda 
 * bu sınıf çalışır.
 *
 * Sınıfın çalışması için gerekli ortam değişkenleri
 * için bkz. 'DATARULES.h' 
 **/
class PlaintoBinary{

 private:
  
  std::ifstream textFile;  // Varsayılan olarak gelen txt dosyası
  std::ofstream dataFile;  // Yazılacak olan binary dosya


  /**
   * Referans .txt dosyasını inceleyip kitap sayısını
   * bulan ve döndüren metod. Birçok teknik olmasına rağmen 
   * biraz daha hantal çalışan ancak doğru sonucu veren
   * 2*'\n' yöntemini kullanır. 
   *
   * 2 tane ardarda '\n' karakteri okunursa kitap sayısı 1 
   * artırılır.
   *
   * --RETURN--
   *   int: Dosyadaki kitap sayısı
   **/
  int book_count();


  /**
   * Text dosyasındaki işaretçiyi, diğer metodların kullanması
   * için, en başa getirir
   **/
  void text_file_begin();

  
  /**
   * Plain text dosyasından bir kitabı tamamen okuyup verileri
   * geri döndüren metod. 
   *
   * --RETURN--
   *   string* : 4 elemanlı kitap verisi. 
   *             [isbn, isim, yazar, fiyat] 
   **/
  std::string* parse_a_book();


  /**
   * parse_a_book metodundan alınan verilerden bir 
   * Book nesnesi oluşturan metod.
   *
   * --ARGS--
   *   strBook : 4 elemanlı kitap verisi.
   *             [isbn, isim, yazar, fiyat]
   *
   * --RETURNS--
   *   Book : txt dosyasından oluşturulmuş Book nesnesi.  
   **/
  Book create_a_book(std::string * strBook);

  
  /**
   * Book nesnesini binary dosyasına yazacak olan metod 
   *
   * --ARGS--
   *   Book : Verilerinin yazılacağı book objesi.
   **/
  void write_a_book(Book book);


  /**
   * Tüm işlemler bittiğinde dosyaların kapatılmasını 
   * metod.
   **/
  void close_files();

  
 public:

  /**
   * Sınıfın özelliklerini (txt ve binary dosyaları)
   * ilklendiren yapıcı metod.
   **/
  PlaintoBinary();

  
  /**
   * Private metodların tamamını kullanarak .txt dosyasındaki
   * tüm kitapları UYGUN BİÇİMDE binary dosyaya yazan metod.
   * 
   * Herhangi bi özelleştirme içermez.  
   **/
  void write_to_data_file();

  
};
#endif
