/**
 * Proje boyunca dosya okuma ve yazma işlemlerinin tamamında
 * kullanılacak veri kurallarının global tanımlarını içeren
 * header dosyası.
 *
 * Yazar: Berkay Dedeoğlu
 * Tarih: 27.02.2018
 * 
 **/


#ifndef DATARULES_H
#define DATARULES_H

#include <string>


// Başlangıçta hazır olarak gelen kitap dosyasının (plain text) yolu ve ismi.  
const std::string TEXTFILENAME = "../data/Books.txt";


// Verilerin tutulduğu ve üzerinde işlemlerin yapıldıığı (binary) dosyanın yolu ve ismi.
const std::string DATAFILENAME = "../data/Books.bin";


// Kitap kayıtlarının sabit uzunluklu olması için kullanılan boş karakter. bkz. Book::serialize()
const char BLANK_CHAR = 1;


// Verilerin tutulduğu dosyadaki sabit uzunlukları
const int STATE_SIZE = 1;
const int ISBN_SIZE = 20;
const int NAME_SIZE = 200;
const int AUTHOR_SIZE = 100;
const int COST_SIZE = 10;

#endif

