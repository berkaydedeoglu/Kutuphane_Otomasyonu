/**
 * Yazar: Berkay Dedeoğlu
 * Tarih: 01.03.2018
 **/


#ifndef BOOKSIOHELPER_H
#define BOOKSIOHELPER_H

#include <fstream>
#include <string>
#include "Book.h"

/**
 * Kitap verilerinin tutulduğu binary dosyaya erişimi sağlayan
 * sınıftır.
 *
 * Proje boyunca ikili dosyaya sadece buradan erişim sağlanır.
 * Dosya bu sınıf ile projenin diğer modüllerinden 
 * soyutlanmıştır.
 *
 * Sınıfla sadece Book nesneleri kullanarak iletişim kurabilirsiniz.
 **/
class BooksIOHelper{

 private:
  std::fstream book_file;
  unsigned int BUFFER_SIZE;
  unsigned int total_book_count;
  

  /**
   * DATARULES.h dosyasından alınan veri boyutlarını
   * toplayan metod.
   *
   * --Return--
   *   int: Bir verinin dosyadaki toplam boyutu (byte)
   **/
  int buffer_size();


  /**
   * Dosya işaretçisini, verilen kitap verisinin
   * başına koyan metod.
   *
   * --Input--
   *   int: Gidilmesi istenen kitabın sıra numarası 
   **/
  void go_record(unsigned int record_index);

 public:

  /**
   * Sadece özellikler ilklendirilir.
   **/
  BooksIOHelper();


  /**
   * Binary veri dosyasını açan metod.
   *
   * --Return--
   *    bool: Dosyanın açılıp açılamadığı durumu. 
   **/
  bool open_file();


  /**
   * Dosyadan istenilen sıradaki veriyi alıp Book 
   * nesnesine dönüştüren metoddur.
   *
   * Verinin silinmiş olması bu metodu etkilemez.
   *
   * --Input--
   *   int index: İstenilen kitabın indexsi.
   *
   * --Return--
   *   Book: İstenilen sıradaki verinin Book nesnesi. 
   **/
  Book get_record(unsigned int index);

  /**
   * Dosyadan kitapların yalnızca isimlerinin alınmasını sağlar.
   * Bu isimler ve metodun çağrılmasını sağlayan indexler sayesinde
   * sıralama ve arama işlemleri daha hızlı yapılır.
   *
   * --Input--
   *   unsigned int index: İsmin alınmak istendiği kaydın sıra numarası
   *
   * --Return--
   *   std::string: İstenilen kitabın yalnızca ismi.
   **/
  std::string get_just_name(unsigned int index);

  /**
   * Dosyadan kitapların yalnızca isbn numaralrının alınmasını sağlar.
   * Bu isimler ve metodun çağrılmasını sağlayan indexler sayesinde
   * sıralama ve arama işlemleri daha hızlı yapılır.
   *
   * --Input--
   *   unsigned int index: isbn numarasının alınmak istendiği kaydın sıra numarası
   *
   * --Return--
   *   std::string: İstenilen kitabın yalnızca isbn numarası.
   **/
  std::string get_just_isbn(unsigned int index);

  
  /**
   * Veri dosyasındaki kitapların sayısını bulan 
   * metod.
   *
   * Verilerin silinmiş olması bu metodu etkilemez.
   *
   * --Return--
   *  int: Dosyadaki tüm verilerin kitap cinsinden adetdi.
   **/
  unsigned int get_count();


  /**
   * Veri dosyasının içindeki silinmiş (state değeri 'd' olan)
   * kitapların sayısını döndüren metod.
   *
   * Lineer arama yaptığı için düşük performanslıdır. Ancak 
   * verileri Book olarak almaz. Her kayıt için 1 veri okuyarak
   * kontrol eder.
   *
   * --Return--
   *   int: Dosyadaki silinmmiş kayıtların sayısı. 
   **/
  unsigned int get_deleted_count();


  /**
   * Dosyanın sonuna yeni kitap verisi ekleyen metod.
   *
   * --Input--
   *   Book: Dosyaya yazılması istenen Book nesnesi.
   **/
  void add_book(Book book);


  /**
   * Dosyada verilen index üzerindeki kayıdın 
   * üzerine yazarak yeni kayıt oluşturan metod.
   *
   * Güncelleme yapmak ya da silinen veri üzerine 
   * yazmak için kullanılabilir.
   *
   * --Inputs--
   *   Book: Yazılması istenen kitap nesnesi
   *   index: Üzerine yazılan kitabın dosya indeksi.
   **/
  void write_book(Book book, unsigned int index);


  /**
   * Dosya üzerindeki indexi verilen kaydın durum değerini
   * değiştiren metod.
   *
   * Gerçek anlamda diskten silme işlemi yapmaz. Durum değerini
   * 'd' (deleted) yapar.
   *
   * --Input--
   *   int: Silinme işaretçisinin değiştirilmesi istenen kayıt.
   **/
  void delete_book(unsigned int index);


  /**
   * Verilen iki indexteki verilerin yerini değiştiren 
   * metod.
   *
   * --Input--
   *   int: Yerinin değiştirilmesi istenen kaydın sıra numarası
   *   int: Yerinin değiştirilmesi istenen kaydın sıra numarası
   **/
  void swap_records(unsigned int index_1, unsigned int index_2);

  /**
   * Veri dosyasında silinen veriler silinme işaretçisiyle
   * işaretlenir. Ancak bu veriler hala dosyada tutulmaya devam
   * eder. 
   *
   * Bu metod bu verileri yeniden düzenleyerek silinen dosyaları 
   * hafızadan gerçekten siler.
   *
   * Metod çalışırken diskte 2 dosya tutulur.
   *
   * --Return--
   *  TODO: Bilinmiyor.
   **/
  unsigned int compress_file();

  /**
   * Tamponda bekleyen veriyi fiziksel belleğe aktaran 
   * bilinen tanımıyla kaydeden method. 
   *
   * flush() metodunun soyutlanmış halidir.
   **/
  void save_file();

  /**
   * Dosyanın kapatılması ve sıkıştırılmasını sağlayan metod.
   **/
  void close_file();
};
#endif
