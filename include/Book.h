/**
 * Yazar: Berkay Dedeoğlu
 * Tarih: 27.02.2018
 **/


#ifndef BOOK_H
#define BOOK_H

#include <string>


/**
 * İkincil hafızadaki her kitabın birincil hafızaya alındığında
 * bilgilerinin tutulduğu ve bu tip hafıza transfer işlemlerinin
 * gerçekleşmesinin sağlandığı sınıftır.
 *
**/
class Book {
  private:

  std::string state;  // Kitabın silinmiş olup olmadığı (n/d) (normal/deleted)
  std::string isbn;
  std::string name;
  std::string author;
  std::string cost;



  /**
   * Kitabın özelliklerinin dosyaya yazılmak için hazır duruma 
   * getirildiği method. Dosyaya yazılırken tüm özellikler belirli 
   * bir düzende, uzunlukta ve sırada yazılır.
   * 
   * --Inputs--
   *   string str: Düzenlenmesi beklenen özellik.
   *   int size  : Her özelliğin byte uzunluğu bellidir. Bkz. DATARULES.h
   *
   * --Return--
   *   string : Özelliğin dosyaya yazılabilecek hali.
   **/
  std::string fill_string(std::string str, int size);

  
 public:
  
  /**
   * Projenin tamamında sınıfın kullanımını kolaylaştırmak için kullanılabilecek 
   * constructor. 
   **/
  Book();

  
  /**
   * İlklendirilmelerin yapıldığı method.
   * 
   * state durumu varsayılan olarak 'n' (normal) dir.
   **/
  Book(std::string isbn, std::string name, std::string author, std::string cost);
  



  /**
   * Sınıfın taşıdığı kitabın gerçek anlamda dosya yazımına hazırlandığı
   * methoddur. 
   *
   * Sınıfın tüm özellikleri fill_string methoduna verilir ve birleştirilir.
   *
   * Returns:
   *   string : 325 byte uzunluğunda dosyaya yazılmaya hazır string.
   **/
  std::string serialize();
  


  /**
   * Nesne oluşturabilen bir static method. 
   * Dosyadan alınan serialized kitap stringini 
   * Book nesnesine dönüştürür.
   *
   * --Inputs--
   *   string serialized: Daha önce serialized fonksiyonundan çıkmış 
   *              ya da dosyadan alınmış ham 325 byte uzunlukta string 
   *              veri.
   *
   * --Return--
   *   Book : Ham stringten elde edilmiş, RAM üzerinede yaşayabilecek
   *        bir Book nesnesi.
   */
   void deserialize(std::string serialized);
   
   
   
   // Sınıf özelliklerinin getter ve setter metodları
   std::string get_state();
   std::string get_isbn();
   std::string get_name();
   std::string get_author();
   std::string get_cost();

   void set_state(std::string);
   void set_isbn(std::string);
   void set_name(std::string);
   void set_author(std::string);
   void set_cost(std::string);
   // Sınıf özelliklerinin getter ve setter metodları



   /**
   * Kitabın özellikleri dosyaya yazılırken sabit uzunluklu 
   * hale getirilir. 
   *
   * Daha sonra bu veri dosyadan okunduğunda eklenen gereksiz 
   * karakterlerin silinmesini sağlayan method.
   * 
   *
   * --Inputs--
   *    string str: Soyulması gereken veri. Kitap nesnesi degil!!!
   * 
   * 
   * --Return--
   *    string: Verinin okunabilir hali 
   *
   **/
  static std::string strip_str(std::string str);
};
#endif
